
<a name="v1.0.9"></a>
## [v1.0.9](https://gitlab.com/pantacor/pvwificonnect/compare/v1.0.8...v1.0.9)

> 2024-08-23

### Fix

* only show wifi networks an not ethernet network


<a name="v1.0.8"></a>
## v1.0.8

> 2024-08-23

### Feature

* wait 10 cycles for network to be connected if not start ap
* wait for some socket responses
* update container-ci to create new pvpkg_install and gitlab-ci configuration
* use PV_VOLUME_IMPORTS to mount /run/dbus instead of LXC_EXTRA_CONF
* read dockerplatform from src.json
* add script to create pvr export files
* add connman network manager dbus implementation
* wait for dbus to be ready and wait for network to be ready before starting server
* wait for connection to be active before getting ip address of new connection
* add scan network feature
* start hotspot if no wifi doesn't exists
* add freedesktopnm list networks, and connect to new network
* add basic structure for freedesktop network manager

### Fix

* available connection parsing an server starting
* correct index.html addr pointer

