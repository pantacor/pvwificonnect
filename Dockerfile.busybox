FROM --platform=$BUILDPLATFORM golang:1.21.3-alpine3.18 as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR /app
COPY go.mod /app/
COPY go.sum /app/
COPY main.go /app/
COPY modules /app/modules
COPY build.sh /app/

RUN /app/build.sh

FROM --platform=$BUILDPLATFORM amd64/alpine:3.18.4 as compresor

RUN apk add && \
  apk add upx

WORKDIR /app
COPY --from=builder /app/pvwificonnect /app/pvwificonnect
RUN upx pvwificonnect

FROM --platform=$TARGETPLATFORM alpine:3.18 as busybox

ARG TARGETPLATFORM
ARG BUILDPLATFORM

# Create a directory to hold BusyBox
WORKDIR /download
COPY download_busybox.sh /download_busybox.sh
RUN sh /download_busybox.sh

FROM --platform=$TARGETPLATFORM scratch

VOLUME [ "/tmp" ]

WORKDIR /app
COPY --from=compresor /app/pvwificonnect /app/pvwificonnect

WORKDIR /usr/bin/
COPY --from=busybox /download/busybox /busybox/busybox
COPY --from=busybox /download/qemu/* /usr/bin/

COPY static /app/static/
COPY templates /app/templates/ 

RUN ["/busybox/busybox", "--install", "/usr/bin"]
RUN ["/busybox/busybox", "rm", "-f", "/usr/bin/qemu*"]

CMD [ "/app/pvwificonnect" ]
