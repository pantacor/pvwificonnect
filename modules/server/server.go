package server

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"sync/atomic"
	"time"

	"gitlab.com/pantacor/pvwificonnect/modules/endpoints"
	"gitlab.com/pantacor/pvwificonnect/modules/models"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

type key int

const (
	requestIDKey key = 0
)

var (
	healthy int32
)

var functions template.FuncMap = template.FuncMap{
	"notNil": notNil,
}

const enableRequestLog = false

type ServerConfig struct {
	ListenAddr      string
	HttpsListenAddr string
	Logger          *log.Logger
	NmClient        nmutils.NetworkManagerDbus
	TemplatePaths   []string
}

func Serve(conf *ServerConfig) {
	conf.Logger.Println("Server is starting...")

	t := template.New("").Funcs(functions)
	tmpl, err := t.ParseFiles(conf.TemplatePaths...)
	if err != nil {
		conf.Logger.Fatalf("Can't load templates: %v\n", err)
	}
	opts := &models.ServerOptions{
		Logger:     conf.Logger,
		Client:     conf.NmClient,
		TpmlEngine: tmpl,
	}
	router := http.NewServeMux()
	router.Handle("/", endpoints.RedirectToConnect(opts))
	router.Handle("/captive-portal", endpoints.RedirectToConnect(opts))
	router.Handle("/connect", endpoints.HandleIndex(opts))
	router.Handle("/success", endpoints.HandleSuccess(opts))
	router.Handle("/connections", endpoints.HandleConnections(opts))
	router.Handle("/healthz", healthz())
	router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	nextRequestID := func() string {
		return fmt.Sprintf("%d", time.Now().UnixNano())
	}

	middleware := tracing(nextRequestID)(
		logging(conf.Logger)(router),
	)

	server := &http.Server{
		Addr:         conf.ListenAddr,
		Handler:      middleware,
		ErrorLog:     conf.Logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	serverHttps := &http.Server{
		Addr:         conf.HttpsListenAddr,
		Handler:      middleware,
		ErrorLog:     conf.Logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		conf.Logger.Println("Server is shutting down...")
		atomic.StoreInt32(&healthy, 0)

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		server.SetKeepAlivesEnabled(false)
		serverHttps.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			conf.Logger.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
		if err := serverHttps.Shutdown(ctx); err != nil {
			conf.Logger.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
		close(done)
	}()

	atomic.StoreInt32(&healthy, 1)

	go func() {
		conf.Logger.Println("Server is ready to handle requests at", conf.ListenAddr)
		if err := server.ListenAndServe(); err != nil {
			conf.Logger.Fatalf("Could not listen on %s: %v\n", server.Addr, err)
		}
	}()

	go func() {
		conf.Logger.Println("Server is ready to handle https requests at", conf.HttpsListenAddr)
		if err := serverHttps.ListenAndServe(); err != nil {
			conf.Logger.Fatalf("Could not listen on %s: %v\n", serverHttps.Addr, err)
		}
	}()

	<-done
	conf.Logger.Println("Server stopped")
}

func notNil(a interface{}) bool {
	return !reflect.ValueOf(a).IsNil()
}

func healthz() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if atomic.LoadInt32(&healthy) == 1 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}

func logging(logger *log.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if enableRequestLog {
					requestID, ok := r.Context().Value(requestIDKey).(string)
					if !ok {
						requestID = "unknown"
					}
					logger.Println(requestID, r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())
				}
			}()
			next.ServeHTTP(w, r)
		})
	}
}

func tracing(nextRequestID func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestID := r.Header.Get("X-Request-Id")
			if requestID == "" {
				requestID = nextRequestID()
			}
			ctx := context.WithValue(r.Context(), requestIDKey, requestID)
			w.Header().Set("X-Request-Id", requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func logHeaders(opts *models.ServerOptions) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if !strings.Contains(r.URL.Path, "static/") {
				opts.Logger.Println("The request domain is", r.Host)
				opts.Logger.Println("The request path is", r.URL.Path)
				opts.Logger.Println("The request URI is", r.RequestURI)
				opts.Logger.Println("Remote address:", r.RemoteAddr)
				opts.Logger.Println(r.Method)
				for k, v := range r.Header {
					opts.Logger.Printf("%s: %v\n", k, v)
				}
			}
			next.ServeHTTP(w, r)
		})
	}
}
