package nmutils

import (
	"time"
)

type WifiConn struct {
	ID        string   `json:"id"`
	Name      string   `json:"name"`
	Strength  int32    `json:"strength"`
	Frequency int      `json:"frequency"`
	BitRate   int      `json:"bitrate"`
	Security  []string `json:"security"`
	Saved     bool     `json:"saved"`
	A         string   `json:"-"`
	B         int32    `json:"-"`
	C         int32    `json:"-"`
}

type Ip struct {
	Addr string
	Mask int32
}
type IPConfiguration struct {
	Ips      []Ip
	Gw       []Ip
	DnsAddrs []Ip
}

type NetworkManagerDbus interface {
	GetAvailableWiFiNetworks(rescan bool, waitRescan bool) ([]WifiConn, error)
	EnableInterface(name string) error
	ScanAvailableWiFiNetworks() error
	ConnectWiFiNetwork(name string, pass string, freq int, auto bool) error
	ConnectToStoredWiFiNetwork(name string) error
	GetInterfaceIpConf(name string) (*IPConfiguration, error)
	RemoveStoredWiFiNetwork(name string) error
	GetStoredWiFiNetworks() ([]string, error)
	StopAp() error
	StartAp(name, pass string) error
	InitNetwork(watch bool, interval time.Duration) error
}
