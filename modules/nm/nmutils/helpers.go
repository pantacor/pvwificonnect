package nmutils

import (
	"fmt"
	"time"

	"github.com/godbus/dbus/v5"
)

const (
	SystemDbusWaitTime = 60
)

// GetSystemDbus get system dbus
func GetSystemDbus() (conn *dbus.Conn, err error) {
	for i := 0; i < SystemDbusWaitTime; i++ {
		conn, err = dbus.ConnectSystemBus()
		if err != nil {
			fmt.Println("Waiting for system dbus to be ready")
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}

	return conn, err
}
