package freedesktopnm

import (
	"fmt"
	"log"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

const ID = "org.freedesktop.NetworkManager"

const (
	dbusDest          = "org.freedesktop.NetworkManager"
	dbusDestPath      = "/org/freedesktop/NetworkManager"
	dbusCommandPrefix = "org.freedesktop.NetworkManager"
	dbusGetProperty   = "org.freedesktop.DBus.Properties.Get"
	dbusSetProperty   = "org.freedesktop.DBus.Properties.Set"
	getAWNKey         = "networks"
)

var cache map[string]interface{} = map[string]interface{}{}
var ifDevicesCache = map[string]string{}

type FreeDesktopNM struct {
	logger     *log.Logger
	serverPort string
}

func New(logger *log.Logger, port string) *FreeDesktopNM {
	return &FreeDesktopNM{
		serverPort: port,
		logger:     logger,
	}
}

// Public Methods

func (nm *FreeDesktopNM) GetAvailableWiFiNetworks(rescan bool, waitRescan bool) (conns []nmutils.WifiConn, err error) {
	return nm.getAvailableWiFiNetworks("wlan0", rescan, waitRescan)
}

func (nm *FreeDesktopNM) EnableInterface(name string) error {
	return nm.enableInterface(name)
}

func (nm *FreeDesktopNM) ScanAvailableWiFiNetworks() error {
	return nm.scanAvailableWiFiNetworks()
}

func (nm *FreeDesktopNM) ConnectWiFiNetwork(name string, pass string, freq int, auto bool) error {
	return nm.connectWiFiNetwork(name, pass, freq, auto)
}

func (nm *FreeDesktopNM) ConnectToStoredWiFiNetwork(name string) error {
	return nm.connectToStoredWiFiNetwork(name)
}

func (nm *FreeDesktopNM) GetInterfaceIpConf(name string) (*nmutils.IPConfiguration, error) {
	return nm.getInterfaceIpConf(name)
}

func (nm *FreeDesktopNM) RemoveStoredWiFiNetwork(name string) error {
	return nm.removeStoredWiFiNetwork(name)
}

func (nm *FreeDesktopNM) GetStoredWiFiNetworks() ([]string, error) {
	settings, err := nm.getStoredWiFiNetworks()
	if err != nil {
		return nil, nil
	}

	networks := []string{}
	for _, n := range settings {
		networks = append(networks, n.Wireless.Ssid)
	}

	return networks, nil
}

func (nm *FreeDesktopNM) StopAp() error {
	return nm.stopAp()
}

func (nm *FreeDesktopNM) StartAp(name string, pass string) error {
	return nm.startAp(name, pass)
}

func (nm *FreeDesktopNM) InitNetwork(watch bool, interval time.Duration) error {
	return nm.initNetwork(watch, interval)
}

// Private Methods

func (nm *FreeDesktopNM) enableInterface(name string) error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	// Get the NetworkManager object
	obj := conn.Object(dbusDest, dbus.ObjectPath(dbusDestPath))

	// Enable the specified network interface (e.g., wlan0)
	if err := obj.Call("org.freedesktop.NetworkManager.EnableInterface", 0, name).Err; err != nil {
		nm.logger.Println("Failed to enable wlan0:", err)
		return err
	}

	// Disable power-saving mode for the specified device (e.g., wlan0)
	if err := obj.Call("org.freedesktop.NetworkManager.SetDeviceProperty", 0, name, "powersave", dbus.MakeVariant(false)).Err; err != nil {
		nm.logger.Println("Failed to disable power-saving mode for wlan0:", err)
	}

	return err
}

func (nm *FreeDesktopNM) connectToStoredWiFiNetwork(name string) error {
	panic("not implemented connectToStoredWiFiNetwork") // TODO: Implement
}

func (nm *FreeDesktopNM) removeStoredWiFiNetwork(name string) error {
	panic("not implemented removeStoredWiFiNetwork") // TODO: Implement
}

func (nm *FreeDesktopNM) scanAvailableWiFiNetworks() error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	ifName := "wlan0"
	if _, ok := ifDevicesCache[ifName]; !ok {
		ifDevicesCache[ifName], err = nm.getDeviceByIpIface(ifName)
		if err != nil {
			return err
		}
	}

	opts := map[string]interface{}{}
	obj := conn.Object(dbusDest, dbus.ObjectPath(ifDevicesCache[ifName]))
	cmd := fmt.Sprintf("%s.%s", dbusCommandPrefix, "Device.Wireless.RequestScan")
	err = obj.Call(cmd, 0, opts).Store()
	if err != nil {
		return err
	}

	return nil
}

func (nm *FreeDesktopNM) initNetwork(watch bool, interval time.Duration) (err error) {
	ifName := "wlan0"
	started := false
	var devicePath string

	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	for !started {
		devicePath, err = nm.getDeviceByIpIface(ifName)
		if err != nil {
			nm.logger.Println(err.Error())
		}

		if devicePath != "" && devicePath != "/" {
			started = true
			continue
		}
		nm.logger.Printf("Waiting for %s to be available\n", ifName)
		time.Sleep(interval)
	}

	ifDevicesCache[ifName] = devicePath

	mode, err := nm.getDeviceMode(nil, devicePath)
	if err != nil {
		nm.logger.Println("error getDeviceMode:", err)
		return err
	}

	nm.logger.Println("device mode:", mode)
	if mode == 3 {
		nm.logger.Println("disabiling AP to scan networks")
		err = nm.stopAp()
		if err != nil {
			nm.logger.Println("error stopAp:", err)
			return err
		}
	}

	loading := true
	i := 0
	var conns []nmutils.WifiConn
	for loading {
		conns, err = nm.getAvailableWiFiNetworks(ifName, true, true)
		if err != nil {
			nm.logger.Println("error getAvailableWiFiNetworks:", err)
			return err
		}

		if len(conns) > 0 || i > 20 {
			loading = false
		}
		i++
	}

	i = 0
	state := -1
	for i < 20 && state != 2 {
		var activeConnection string
		obj := conn.Object(dbusDest, dbus.ObjectPath(devicePath))
		if variant, err := obj.GetProperty(dbusDest + ".Device.ActiveConnection"); err != nil {
			nm.logger.Println("err Device.ActiveConnection: ", err)
			return err
		} else {
			variant.Store(&activeConnection)
		}

		if activeConnection == "" || activeConnection == "/" {
			err = nil
			break
		}

		nm.logger.Printf("Waiting for connection %s to be active\n", activeConnection)
		state, err = nm.getConnetionState(conn, activeConnection)
		i++
		time.Sleep(1 * time.Second)
	}

	if err != nil {
		return err
	}

	ipConf, err := nm.getInterfaceIpConf(ifName)
	if err != nil {
		nm.logger.Println("error getInterfaceIpConf:", err)
		return err
	}

	if len(ipConf.Ips) == 0 && mode != 3 {
		err = nm.startAp(nmutils.SSID, nmutils.PASS)
		if err != nil {
			nm.logger.Println("error startAp:", err)
			return err
		}

		nm.logger.Println("No network configuration found, creating hotspot ", nmutils.SSID)
		nm.logger.Println("Connect to network using the password: ", nmutils.PASS)
		nm.logger.Printf("You can now enter to: http://10.42.0.1%s\n", nm.serverPort)
		return err
	}

	mode, err = nm.getDeviceMode(nil, devicePath)
	if err != nil {
		nm.logger.Println("error getDeviceMode:", err)
		return err
	}

	if mode == 3 && len(conns) <= 1 {
		err = nm.stopAp()
		if err != nil {
			nm.logger.Println("error stopAp:", err)
			return err
		}

		_, err = nm.getAvailableWiFiNetworks(ifName, true, true)
		if err != nil {
			nm.logger.Println("error getAvailableWiFiNetworks:", err)
			return err
		}

		err = nm.startAp(nmutils.SSID, nmutils.PASS)
		if err != nil {
			nm.logger.Println("error startAp:", err)
			return err
		}

		nm.logger.Println("No network configuration found, creating hotspot ", nmutils.SSID)
		nm.logger.Println("Connect to network using the password: ", nmutils.PASS)
		nm.logger.Printf("You can now enter to: http://10.42.0.1%s\n", nm.serverPort)

		return err
	}

	return err
}
