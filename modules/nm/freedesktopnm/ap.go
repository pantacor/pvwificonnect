package freedesktopnm

import (
	"strings"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

var hotSpotUUID = "e01b5e3f-6308-407a-b455-db55bda88e4a"

func (nm *FreeDesktopNM) stopAp() error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	settingPath, err := nm.getConnectionByUUID(conn, hotSpotUUID)
	if err != nil {
		return err
	}

	if settingPath == "" || settingPath == "/" {
		return nil
	}

	nm.logger.Println("stoping hostspot")

	obj := conn.Object(dbusDest, dbus.ObjectPath(settingPath))
	obj.Call(dbusDest+".Settings.Connection.Delete", 0)

	return err
}

func (nm *FreeDesktopNM) startAp(name string, pass string) error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	settingPath, err := nm.getConnectionByUUID(conn, hotSpotUUID)
	if err != nil {
		nm.logger.Println("Failed getConnectionByUUID:", err)
		return err
	}

	if settingPath == "" {
		setting := createApSettings(name, pass)
		settingPath, err = nm.createConnection(conn, setting)
		if err != nil {
			nm.logger.Println("Failed createConnection:", err)
			return err
		}
	}

	ifName := "wlan0"
	if _, ok := ifDevicesCache[ifName]; !ok {
		ifDevicesCache[ifName], err = nm.getDeviceByIpIface(ifName)
		if err != nil {
			nm.logger.Println("Failed getDeviceByIpIface:", err)
			return err
		}
	}

	_, err = nm.activateConnection(conn, settingPath, ifDevicesCache[ifName])
	if err != nil {
		nm.logger.Println("Failed activateConnection:", err)
		return err
	}

	return nil
}

func (nm *FreeDesktopNM) getConnectionByUUID(conn *dbus.Conn, uuid string) (settingPath string, err error) {
	if conn == nil {
		conn, err = nmutils.GetSystemDbus()
		if err != nil {
			nm.logger.Println("Failed to connect to session bus:", err)
			return settingPath, err
		}
	}

	var response string
	obj := conn.Object(dbusDest, dbus.ObjectPath(dbusDestPath+"/Settings"))
	cmd := dbusCommandPrefix + ".Settings.GetConnectionByUuid"
	err = obj.Call(cmd, 0, uuid).Store(&response)
	if err != nil {
		if strings.Contains(err.Error(), "No connection with the UUID was found") {
			return settingPath, nil
		}
		return settingPath, err
	}

	settingPath = response
	return settingPath, err
}

func createApSettings(name string, pass string) *Setting {
	return &Setting{
		Connection: Connection{
			ID:          name,
			Type:        "802-11-wireless",
			UUID:        hotSpotUUID,
			AutoConnect: true,
		},
		Wireless: Wireless{
			Mode:     "ap",
			Security: "802-11-wireless-security",
			Ssid:     name,
			Band:     "bg",
			Channel:  1,
		},
		Security: WirelessSecurity{
			KeyMgmt: "wpa-psk",
			Psk:     pass,
		},
		IPv4: IPConfig{
			Method: "shared",
		},
		IPv6: IPConfig{
			Method: "ignore",
		},
	}
}
