package freedesktopnm

import (
	"fmt"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

func (nm *FreeDesktopNM) getDeviceMode(conn *dbus.Conn, devicePath string) (device int, err error) {
	if conn == nil {
		conn, err = nmutils.GetSystemDbus()
		if err != nil {
			nm.logger.Println("Failed to connect to session bus:", err)
			return device, err
		}

		defer conn.Close()
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(devicePath))
	if variant, err := obj.GetProperty(dbusDest + ".Device.Wireless.Mode"); err != nil {
		return device, err
	} else {
		variant.Store(&device)
	}

	return device, nil
}

func (nm *FreeDesktopNM) createConnection(conn *dbus.Conn, setting *Setting) (path string, err error) {
	settingsPath := fmt.Sprintf("%s/%s", dbusDestPath, "Settings")
	if conn == nil {
		conn, err = nmutils.GetSystemDbus()
		if err != nil {
			nm.logger.Println("Failed to connect to session bus:", err)
			return path, err
		}
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(settingsPath))
	request := setting.DbusVariantMarshal()
	if err = obj.Call(dbusDest+".Settings.AddConnection", 0, request).Store(&path); err != nil {
		nm.logger.Printf("Failed %s: %s\n", dbusDest+".Settings.AddConnection", err)
		nm.logger.Printf("Request: %+v\n", request)
		return path, err
	}

	return path, nil
}

func (nm *FreeDesktopNM) activateConnection(conn *dbus.Conn, settingPath, devicePath string) (connPath string, err error) {
	if conn == nil {
		conn, err = nmutils.GetSystemDbus()
		if err != nil {
			nm.logger.Println("Failed to connect to session bus:", err)
			return connPath, err
		}
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(dbusDestPath))
	connPathObj := dbus.ObjectPath(settingPath)
	devicePathObj := dbus.ObjectPath(devicePath)
	specficPathObj := dbus.ObjectPath("/")
	if err = obj.Call(dbusDest+".ActivateConnection", 0, connPathObj, devicePathObj, specficPathObj).Store(&connPath); err != nil {
		nm.logger.Printf("Failed %s: %s\n", dbusDest+".ActivateConnection", err)
		return connPath, err
	}

	return connPath, err
}
