package freedesktopnm

import (
	"fmt"
	"strings"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

func (nm *FreeDesktopNM) getInterfaceIpConf(name string) (ipConf *nmutils.IPConfiguration, err error) {
	ipConf = &nmutils.IPConfiguration{
		Ips:      []nmutils.Ip{},
		Gw:       []nmutils.Ip{},
		DnsAddrs: []nmutils.Ip{},
	}
	device, ok := ifDevicesCache[name]
	if !ok {
		device, err = nm.getDeviceByIpIface(name)
		if err != nil {
			return ipConf, err
		}

		ifDevicesCache[name] = device
	}

	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return ipConf, err
	}
	defer conn.Close()

	var ip4Conf string
	obj := conn.Object(dbusDest, dbus.ObjectPath(device))
	if variant, err := obj.GetProperty(dbusDest + ".Device.Ip4Config"); err != nil {
		nm.logger.Printf("Failed %s: %s\n", dbusDest+".Device.Ip4Config", err)
		return ipConf, err
	} else {
		variant.Store(&ip4Conf)
	}

	var ip6Conf string
	obj = conn.Object(dbusDest, dbus.ObjectPath(device))
	if variant, err := obj.GetProperty(dbusDest + ".Device.Ip6Config"); err != nil {
		nm.logger.Printf("Failed %s: %s\n", dbusDest+".Device.Ip6Config", err)
		return ipConf, err
	} else {
		variant.Store(&ip6Conf)
	}

	if ip4Conf != "" && ip4Conf != "/" {
		var ip4Address []interface{}
		obj = conn.Object(dbusDest, dbus.ObjectPath(ip4Conf))
		if variant, err := obj.GetProperty(dbusDest + ".IP4Config.AddressData"); err != nil {
			nm.logger.Printf("Failed %s: %s\n", dbusDest+".IP4Config.AddressData", err)
			return ipConf, err
		} else {
			variant.Store(&ip4Address)
		}

		var dnsData []interface{}
		obj = conn.Object(dbusDest, dbus.ObjectPath(ip4Conf))
		if variant, err := obj.GetProperty(dbusDest + ".IP4Config.NameserverData"); err != nil {
			nm.logger.Printf("Failed %s: %s\n", dbusDest+".IP4Config.NameserverData", err)
			return ipConf, err
		} else {
			variant.Store(&dnsData)
		}

		var gwData string
		obj = conn.Object(dbusDest, dbus.ObjectPath(ip4Conf))
		if variant, err := obj.GetProperty(dbusDest + ".IP4Config.Gateway"); err != nil {
			nm.logger.Printf("Failed %s: %s\n", dbusDest+".IP4Config.Gateway", err)
			return ipConf, err
		} else {
			variant.Store(&gwData)
		}

		for _, i := range ip4Address {
			ipIf := i.(map[string]interface{})
			ipConf.Ips = append(ipConf.Ips, nmutils.Ip{
				Addr: ipIf["address"].(string),
				Mask: int32(ipIf["prefix"].(uint32)),
			})
		}

		for _, i := range dnsData {
			ipIf := i.(map[string]interface{})
			ipConf.DnsAddrs = append(ipConf.DnsAddrs, nmutils.Ip{
				Addr: ipIf["address"].(string),
				Mask: int32(0),
			})
		}

		ipConf.Gw = append(ipConf.Gw, nmutils.Ip{Addr: gwData, Mask: 0})
	}

	return ipConf, nil
}

func (nm *FreeDesktopNM) getDeviceByIpIface(name string) (device string, err error) {
	if name == "" {
		name = "wlan0"
	}
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return device, err
	}
	defer conn.Close()

	var s string
	obj := conn.Object(dbusDest, dbus.ObjectPath(dbusDestPath))
	err = obj.Call(fmt.Sprintf("%s.%s", dbusCommandPrefix, "GetDeviceByIpIface"), 0, name).Store(&s)

	device = s

	if device == "" {
		return device, fmt.Errorf("%s is not found on devices", name)
	}

	return device, err
}

func (nm *FreeDesktopNM) getWifiNeworksOfDevice(device string, rescan, waitRescan bool) (conns []nmutils.WifiConn, err error) {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return conns, err
	}
	defer conn.Close()

	nm.logger.Println("Getting available wifi networks from: ", device)

	var lastScan int
	obj := conn.Object(dbusDest, dbus.ObjectPath(device))
	if variant, err := obj.GetProperty(dbusDest + ".Device.Wireless.LastScan"); err != nil {
		return conns, err
	} else {
		variant.Store(&lastScan)
	}

	if rescan {
		opts := map[string]interface{}{}
		obj = conn.Object(dbusDest, dbus.ObjectPath(device))
		nm.logger.Println("Scaning the network for access points")
		cmd := fmt.Sprintf("%s.%s", dbusCommandPrefix, "Device.Wireless.RequestScan")
		err = obj.Call(cmd, 0, opts).Store()
		if err != nil && strings.Contains(err.Error(), "Scanning not allowed while already scanning") {
			if !strings.Contains(err.Error(), "Scanning not allowed while already scanning") {
				return conns, err
			}
		}
		waitChange := true
		i := 0
		for waitChange && waitRescan {
			var newLastScan int
			obj = conn.Object(dbusDest, dbus.ObjectPath(device))
			if variant, err := obj.GetProperty(dbusDest + ".Device.Wireless.LastScan"); err != nil {
				return conns, err
			} else {
				variant.Store(&newLastScan)
			}
			nm.logger.Println("Waiting for scan to finish")

			waitChange = lastScan == newLastScan || i > 100
			i = i + 1
			time.Sleep(1 * time.Second)
		}
	}

	var apsResponse []string
	obj = conn.Object(dbusDest, dbus.ObjectPath(device))
	cmd := fmt.Sprintf("%s.%s", dbusCommandPrefix, "Device.Wireless.GetAllAccessPoints")
	err = obj.Call(cmd, 0).Store(&apsResponse)
	if err != nil {
		return conns, err
	}

	var activeConnection string
	obj = conn.Object(dbusDest, dbus.ObjectPath(device))
	if variant, err := obj.GetProperty(dbusDest + ".Device.ActiveConnection"); err != nil {
		fmt.Println("err: ", err)
		return conns, err
	} else {
		variant.Store(&activeConnection)
	}

	var activeAccessPoint string
	if activeConnection != "" && activeConnection != "/" {
		obj = conn.Object(dbusDest, dbus.ObjectPath(activeConnection))
		if variant, err := obj.GetProperty(dbusDest + ".Connection.Active.SpecificObject"); err != nil {
			return conns, err
		} else {
			variant.Store(&activeAccessPoint)
		}
	}

	apPropertyPrefix := fmt.Sprintf("%s.%s", dbusDest, "AccessPoint")
	networkKeys := map[string]nmutils.WifiConn{}
	for _, ap := range apsResponse {
		ssid := []byte{}
		strength := 0
		lastSeen := 0
		flags := 0
		mode := 0
		frequency := 0
		bitrate := 0
		obj := conn.Object(dbusDest, dbus.ObjectPath(ap))
		if variant, err := obj.GetProperty(apPropertyPrefix + ".Ssid"); err != nil {
			return conns, err
		} else {
			variant.Store(&ssid)
		}
		if variant, err := obj.GetProperty(apPropertyPrefix + ".Strength"); err != nil {
			return conns, err
		} else {
			variant.Store(&strength)
		}
		if variant, err := obj.GetProperty(apPropertyPrefix + ".LastSeen"); err != nil {
			return conns, err
		} else {
			variant.Store(&lastSeen)
		}
		if variant, err := obj.GetProperty(apPropertyPrefix + ".RsnFlags"); err != nil {
			return conns, err
		} else {
			variant.Store(&flags)
		}
		if variant, err := obj.GetProperty(apPropertyPrefix + ".Mode"); err != nil {
			return conns, err
		} else {
			variant.Store(&mode)
		}
		if variant, err := obj.GetProperty(apPropertyPrefix + ".Frequency"); err != nil {
			return conns, err
		} else {
			variant.Store(&frequency)
		}
		if variant, err := obj.GetProperty(apPropertyPrefix + ".MaxBitrate"); err != nil {
			return conns, err
		} else {
			variant.Store(&bitrate)
		}
		if len(ssid) == 0 {
			continue
		}

		key := fmt.Sprintf("%s-%d", ssid, frequency)
		current := false
		if ap == activeAccessPoint {
			current = true
		}

		if _, ok := networkKeys[key]; ok && !current {
			continue
		}

		networkKeys[key] = nmutils.WifiConn{
			ID:        ap,
			Name:      string(ssid),
			Strength:  int32(strength),
			Frequency: frequency,
			BitRate:   bitrate,
			Saved:     current,
			Security:  []string{},
		}
	}

	for _, v := range networkKeys {
		conns = append(conns, v)
	}

	return conns, err
}
