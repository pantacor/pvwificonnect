package freedesktopnm

import (
	"fmt"

	"github.com/godbus/dbus/v5"
)

type Wireless struct {
	Mode     string `dbus:"mode"`
	Security string `dbus:"security"`
	Ssid     string `dbus:"ssid"`
	Band     string `dbus:"band"`
	Channel  int    `dbus:"channel"`
}

type WirelessSecurity struct {
	AuthAlg string `dbus:"auth-alg"`
	KeyMgmt string `dbus:"key-mgmt"`
	Psk     string `dbus:"psk"`
}

type IPData struct {
	Address string `dbus:"address"`
	Prefix  int32  `dbus:"prefix"`
}

type IPConfig struct {
	Addressdata []IPData `dbus:"address-data"`
	Addresses   []uint32 `dbus:"addresses"`
	DnsSearch   []string `dbus:"dns-search"`
	DnsData     []string `dbus:"dns-data"`
	Method      string   `dbus:"method"`
	RouteData   []IPData `dbus:"route-data"`
	Routes      []uint32 `dbus:"routes"`
}

type Connection struct {
	ID            string `dbus:"id"`
	InterfaceName string `dbus:"interface-name"`
	Type          string `dbus:"type"`
	UUID          string `dbus:"uuid"`
	AutoConnect   bool   `dbus:"autoconnect"`
}

type Setting struct {
	ConnPath   string           `dbus:"-"`
	Path       string           `dbus:"-"`
	Wireless   Wireless         `dbus:"802-11-wireless"`
	Security   WirelessSecurity `dbus:"802-11-wireless-security"`
	Connection Connection       `dbus:"connection"`
	IPv4       IPConfig         `dbus:"ipv4"`
	IPv6       IPConfig         `dbus:"ipv6"`
}

// var defaultSettingMap = map[string]interface{}{
// 	"802-11-wireless": map[string]interface{}{
// 		"mode":     "",
// 		"security": "802-11-wireless-security",
// 		"ssid":     []byte{},
// 	},
// 	"802-11-wireless-security": map[string]interface{}{
// 		"key-mgmt": "wpa-psk",
// 		"psk":      "",
// 	},
// 	"connection": map[string]interface{}{
// 		"id":             "",
// 		"interface-name": "",
// 		"type":           "802-11-wireless",
// 		"uuid":           "",
// 	},
// 	"ipv4": map[string]interface{}{
// 		"method": "auto",
// 	},
// 	"ipv6": map[string]interface{}{
// 		"method": "disabled",
// 	},
// 	"proxy": map[string]interface{}{},
// }

func (s *Setting) Parse(setting map[string]interface{}) *Setting {
	s.ConnPath = ""
	s.Path = ""
	if wireless, ok := setting["802-11-wireless"]; ok {
		s.Wireless = Wireless{}

		value := wireless.(map[string]interface{})
		if mode, ok := value["mode"]; ok {
			s.Wireless.Mode = mode.(string)
		}
		if security, ok := value["security"]; ok {
			s.Wireless.Security = security.(string)
		}
		if ssid, ok := value["ssid"]; ok {
			s.Wireless.Ssid = fmt.Sprintf("%s", ssid)
		}
	}

	if security, ok := setting["802-11-wireless-security"]; ok {
		s.Security = WirelessSecurity{}

		value := security.(map[string]interface{})
		if authAlg, ok := value["auth-alg"]; ok {
			s.Security.AuthAlg = authAlg.(string)
		}
		if keyMgmt, ok := value["key-mgmt"]; ok {
			s.Security.KeyMgmt = keyMgmt.(string)
		}
		if psk, ok := value["psk"]; ok {
			s.Security.Psk = psk.(string)
		}
	}

	if connection, ok := setting["connection"]; ok {
		s.Connection = Connection{}

		value := connection.(map[string]interface{})
		if id, ok := value["id"]; ok {
			s.Connection.ID = id.(string)
		}
		if interfaceName, ok := value["interface-name"]; ok {
			s.Connection.InterfaceName = interfaceName.(string)
		}
		if t, ok := value["type"]; ok {
			s.Connection.Type = t.(string)
		}
		if t, ok := value["autoconnect"]; ok {
			s.Connection.AutoConnect = t.(bool)
		}
		if uuid, ok := value["uuid"]; ok {
			s.Connection.UUID = uuid.(string)
		}
	}

	if ip, ok := setting["ipv4"]; ok {
		s.IPv4 = IPConfig{}

		value := ip.(map[string]interface{})
		if v, ok := value["method"]; ok {
			s.IPv4.Method = v.(string)
		}
		if v, ok := value["address-data"]; ok {
			ips := v.([]map[string]dbus.Variant)
			s.IPv4.Addressdata = getIpsFromNM(ips)
		}
		if v, ok := value["dns-data"]; ok {
			ips := v.([]string)
			s.IPv4.DnsData = ips
		}
	}

	if ip, ok := setting["ipv6"]; ok {
		s.IPv6 = IPConfig{}

		value := ip.(map[string]interface{})
		if v, ok := value["method"]; ok {
			s.IPv6.Method = v.(string)
		}
		if v, ok := value["address-data"]; ok {
			ips := v.([]map[string]dbus.Variant)
			s.IPv6.Addressdata = getIpsFromNM(ips)
		}
		if v, ok := value["dns-data"]; ok {
			ips := v.([]string)
			s.IPv4.DnsData = ips
		}
	}

	return s
}

type dbusInner map[string]interface{}

func (s *Setting) DbusVariantMarshal() map[string]dbusInner {
	result := map[string]dbusInner{}

	wireless := map[string]interface{}{}
	if s.Wireless.Mode != "" {
		wireless["mode"] = s.Wireless.Mode
	}
	if s.Wireless.Security != "" {
		wireless["security"] = s.Wireless.Security
	}
	if s.Wireless.Ssid != "" {
		wireless["ssid"] = []byte(s.Wireless.Ssid)
	}
	if s.Wireless.Band != "" {
		wireless["band"] = s.Wireless.Band
	}
	if s.Wireless.Channel > 0 {
		wireless["channel"] = s.Wireless.Channel
	}

	security := map[string]interface{}{}
	if s.Security.AuthAlg != "" {
		security["auth-alg"] = s.Security.AuthAlg
	}
	if s.Security.Psk != "" {
		security["psk"] = s.Security.Psk
	}
	if s.Security.KeyMgmt != "" {
		security["key-mgmt"] = s.Security.KeyMgmt
	}

	connection := map[string]interface{}{}
	connection["autoconnect"] = s.Connection.AutoConnect
	if s.Connection.ID != "" {
		connection["id"] = s.Connection.ID
	}
	if s.Connection.InterfaceName != "" {
		connection["interface-name"] = s.Connection.InterfaceName
	}
	if s.Connection.Type != "" {
		connection["type"] = s.Connection.Type
	}
	if s.Connection.UUID != "" {
		connection["uuid"] = s.Connection.UUID
	}

	ipv4 := map[string]interface{}{}
	if s.IPv4.Method != "" {
		ipv4["method"] = s.IPv4.Method
	}
	ipv6 := map[string]interface{}{}
	if s.IPv6.Method != "" {
		ipv6["method"] = s.IPv6.Method
	}

	result["802-11-wireless"] = wireless
	result["802-11-wireless-security"] = security
	result["connection"] = connection
	result["ipv4"] = ipv4
	result["ipv6"] = ipv6

	return result
}

func getIpsFromNM(ips []map[string]dbus.Variant) []IPData {
	ipData := []IPData{}
	for _, ip := range ips {
		var data *IPData
		if addr, ok := ip["address"]; ok {
			addr.Store(&data.Address)
		}
		if prefix, ok := ip["prefix"]; ok {
			prefix.Store(&data.Prefix)
		}
		if data != nil {
			ipData = append(ipData, *data)
		}
	}

	return ipData
}
