package freedesktopnm

import (
	"fmt"
	"strings"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

func (nm *FreeDesktopNM) getStoredWiFiNetworks() (networks []Setting, err error) {
	name := "wlan0"
	device, ok := ifDevicesCache[name]
	if !ok {
		device, err = nm.getDeviceByIpIface(name)
		if err != nil {
			return networks, err
		}
	}
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return networks, err
	}
	defer conn.Close()

	var availableSettings []string
	obj := conn.Object(dbusDest, dbus.ObjectPath(device))
	if variant, err := obj.GetProperty(dbusDest + ".Device.AvailableConnections"); err != nil {
		return networks, err
	} else {
		variant.Store(&availableSettings)
	}

	var activeConns []string
	obj = conn.Object(dbusDest, dbus.ObjectPath(dbusDestPath))
	if variant, err := obj.GetProperty(dbusDest + ".ActiveConnections"); err != nil {
		return networks, err
	} else {
		variant.Store(&activeConns)
	}

	connections := map[string]string{}
	for _, activeConn := range activeConns {
		var c string
		obj = conn.Object(dbusDest, dbus.ObjectPath(activeConn))
		if variant, err := obj.GetProperty(dbusDest + ".Connection.Active.Connection"); err != nil {
			return networks, err
		} else {
			variant.Store(&c)
		}
		connections[c] = activeConn
	}

	networks = []Setting{}
	for _, settingPath := range availableSettings {
		var response map[string]interface{}
		obj = conn.Object(dbusDest, dbus.ObjectPath(settingPath))
		if err = obj.Call(dbusDest+".Settings.Connection.GetSettings", 0).Store(&response); err != nil {
			return networks, err
		}

		setting := new(Setting)
		setting.Parse(response)
		setting.Path = settingPath
		if connPath, ok := connections[settingPath]; ok {
			setting.ConnPath = connPath
		}
		networks = append(networks, *setting)
	}

	return networks, err
}

func (nm *FreeDesktopNM) connectWiFiNetwork(name string, pass string, frequency int, auto bool) (err error) {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	networks, err := nm.getStoredWiFiNetworks()
	if err != nil {
		return err
	}

	var setting *Setting
	for _, n := range networks {
		if name == n.Wireless.Ssid && n.ConnPath != "" {
			setting = &n
			break
		}
	}

	if setting == nil {
		setting = &Setting{
			Connection: Connection{
				ID:            name,
				InterfaceName: "wlan0",
				Type:          "802-11-wireless",
				AutoConnect:   true,
			},
			Wireless: Wireless{
				Mode:     "infrastructure",
				Security: "802-11-wireless-security",
				Ssid:     name,
			},
			Security: WirelessSecurity{
				AuthAlg: "open",
				KeyMgmt: "wpa-psk",
				Psk:     pass,
			},
			IPv4: IPConfig{
				Method: "auto",
			},
			IPv6: IPConfig{
				Method: "ignore",
			},
		}

		response, err := nm.createConnection(conn, setting)
		if err != nil {
			return err
		}

		setting.Path = response
	}

	ifName := setting.Connection.InterfaceName
	if _, ok := ifDevicesCache[ifName]; !ok {
		ifDevicesCache[ifName], err = nm.getDeviceByIpIface(ifName)
		if err != nil {
			return err
		}
	}

	connPath, err := nm.activateConnection(conn, setting.Path, ifDevicesCache[ifName])
	if err != nil {
		return err
	}

	i := 0
	state := -1
	for i < 20 && state != 2 {
		nm.logger.Printf("Waiting for connection %s to be active\n", connPath)
		state, err = nm.getConnetionState(conn, connPath)
		i++
		time.Sleep(1 * time.Second)
	}

	if err != nil {
		return err
	}

	if state != 2 {
		return fmt.Errorf("can't activate connection %s", connPath)
	}

	nm.logger.Printf("Connection activated")

	return nil
}

func (nm *FreeDesktopNM) getConnetionState(conn *dbus.Conn, settingPath string) (state int, err error) {
	if conn == nil {
		conn, err = nmutils.GetSystemDbus()
		if err != nil {
			nm.logger.Println("Failed to connect to session bus:", err)
			return state, err
		}
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(settingPath))
	if variant, err := obj.GetProperty(dbusDest + ".Connection.Active.State"); err != nil {
		fmt.Println(settingPath)
		return state, err
	} else {
		variant.Store(&state)
	}

	return state, err
}

func (nm *FreeDesktopNM) getAvailableWiFiNetworks(name string, rescan bool, waitRescan bool) (conns []nmutils.WifiConn, err error) {
	if name == "" {
		name = "wlan0"
	}
	if _, ok := ifDevicesCache[name]; !ok {
		ifDevicesCache[name], err = nm.getDeviceByIpIface(name)
		if err != nil {
			return conns, err
		}
	}

	devicePath := ifDevicesCache[name]
	mode, err := nm.getDeviceMode(nil, devicePath)
	if err != nil {
		nm.logger.Println(err)
	}

	if mode == 3 && !rescan {
		if c, ok := cache[getAWNKey]; ok {
			conns = c.([]nmutils.WifiConn)
		}
		return conns, err
	}

	conns, err = nm.getWifiNeworksOfDevice(devicePath, rescan, waitRescan)
	if err != nil {
		return conns, err
	}

	// Cache manage
	if len(conns) <= 1 {
		if c, ok := cache[getAWNKey]; ok {
			conns = c.([]nmutils.WifiConn)
		}
	} else {
		cache[getAWNKey] = conns
	}

	networks := []string{}
	for _, conn := range conns {
		networks = append(networks, fmt.Sprintf("%s (%d)", conn.Name, conn.Frequency))
	}

	nm.logger.Println("Networks: ", strings.Join(networks, ", "))

	return conns, nil
}
