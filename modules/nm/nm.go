package nm

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/connman"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/freedesktopnm"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

const (
	NmTypeDonotExist = "network manager not supported"
)

func NewNetworkManagerDbus(t string, logger *log.Logger, port string) (nmutils.NetworkManagerDbus, error) {
	switch t {
	case connman.ID:
		return connman.New(logger, port), nil
	case freedesktopnm.ID:
		return freedesktopnm.New(logger, port), nil
	default:
		return nil, errors.New(NmTypeDonotExist)
	}
}

func GetAvailableDbus() (typeOfDbus string, err error) {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		return typeOfDbus, err
	}
	defer conn.Close()

	var s []string
	networkManagerFound := false
	for i := 0; i < nmutils.SystemDbusWaitTime; i++ {
		obj := conn.Object("org.freedesktop.DBus", dbus.ObjectPath("/org/freedesktop/DBus"))
		err = obj.Call("org.freedesktop.DBus.ListNames", 0).Store(&s)

		for _, service := range s {
			if service == "net.connman" {
				return connman.ID, nil
			}
			if service == "org.freedesktop.NetworkManager" {
				networkManagerFound = true
			}
		}
		if err != nil || !networkManagerFound {
			fmt.Println("waiting for NetworkManager dbus to be ready")
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}

	if !networkManagerFound {
		fmt.Println("unable to determine the type of network manager via D-Bus, defaulting to network manager")
		fmt.Printf("org.freedesktop.DBus.ListNames: %+v\n", s)
	}

	return freedesktopnm.ID, nil
}
