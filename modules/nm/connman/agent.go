package connman

import (
	"fmt"
	"log"

	"github.com/godbus/dbus/v5"
)

type Agent struct {
	Name       string
	Path       dbus.ObjectPath
	Interface  string
	Passphrase string
}

func (a *Agent) Destroy() error {
	conn, err := dbus.SystemBus()
	if err != nil {
		return err
	}

	reply, err := conn.ReleaseName(a.Name)
	if err != nil {
		return err
	}

	if reply != dbus.ReleaseNameReplyReleased {
		return fmt.Errorf("could not release the name")
	}

	return conn.Export(nil, a.Path, a.Interface)
}

func (a *Agent) RequestInput(service dbus.ObjectPath, rq map[string]dbus.Variant) (map[string]dbus.Variant, *dbus.Error) {
	return map[string]dbus.Variant{
		"Passphrase": dbus.MakeVariant(a.Passphrase),
	}, nil
}

func (a *Agent) ReportError(service dbus.ObjectPath, err string) *dbus.Error {
	log.Printf("%s: %s\n", service, err)
	return nil
}
