package connman

import (
	"errors"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

var netCache []nmutils.WifiConn = nil

func getWifiProperty(nm *ConnmanDbus, conn *dbus.Conn, propName string, prop interface{}) (err error) {
	properties := map[string]dbus.Variant{}
	obj := conn.Object(dbusDest, dbus.ObjectPath(wifiPath))
	err = obj.Call("net.connman.Technology.GetProperties", 0).Store(&properties)
	if err != nil {
		nm.logger.Println("error getting wifi properties:", err)
		return err
	}

	if val, ok := properties[propName]; ok {
		if err = val.Store(prop); err != nil {
			nm.logger.Println("error getting wifi property:", propName)
			return err
		}

		return nil
	}

	return nil
}

func getApProperty(nm *ConnmanDbus, conn *dbus.Conn, propName string, prop interface{}) (err error) {
	properties := map[string]dbus.Variant{}
	obj := conn.Object(dbusDest, dbus.ObjectPath(apPath))
	err = obj.Call("net.connman.Technology.GetProperties", 0).Store(&properties)
	if err != nil {
		nm.logger.Println("error getting wifi properties:", err)
		return err
	}

	if val, ok := properties[propName]; ok {
		if err = val.Store(prop); err != nil {
			nm.logger.Println("error getting wifi property:", propName)
			return err
		}

		return nil
	}

	return nil
}

func getWifiIp(nm *ConnmanDbus, conn *dbus.Conn) (conf *nmutils.IPConfiguration, err error) {
	network := nmutils.WifiConn{}
	if connectedTo == nil {
		networks := []nmutils.WifiConn{}
		for i := 0; i < 10; i++ {
			networks, err = getAvailableNetworks(nm, conn, true)
			if err != nil {
				time.Sleep(1 * time.Second)
				continue
			}

			break
		}

		if err != nil {
			nm.logger.Println("error getAvailableNetworks:", err)
			return conf, err
		}

		for _, val := range networks {
			if val.Saved {
				network = val
			}
		}
	} else {
		network = *connectedTo
	}

	if network.ID == "" {
		return conf, errors.New("network not found")
	}

	properties := map[string]dbus.Variant{}
	obj := conn.Object(dbusDest, dbus.ObjectPath(network.ID))
	err = obj.Call("net.connman.Service.GetProperties", 0).Store(&properties)
	if err != nil {
		nm.logger.Println("error getting wifi properties:", err)
		return conf, err
	}

	conf = &nmutils.IPConfiguration{}
	ipv4 := map[string]dbus.Variant{}
	ipv6 := map[string]dbus.Variant{}
	dns := []dbus.Variant{}

	err = properties["IPv4"].Store(&ipv4)
	if err != nil {
		nm.logger.Println("error getting IPv4 properties:", err)
	}
	err = properties["IPv6"].Store(&ipv6)
	if err != nil {
		nm.logger.Println("error getting IPv6 properties:", err)
	}
	err = properties["Nameservers"].Store(&dns)
	if err != nil {
		nm.logger.Println("error getting Nameservers properties:", err)
	}

	addr := ""
	gateway := ""
	addrMask := ""
	mask := int32(32)

	if _, ok := ipv4["Netmask"]; ok {
		err = ipv4["Netmask"].Store(&addrMask)
		if err != nil {
			nm.logger.Println("error getting Netmask properties:", err)
		}

		mask, err = netmaskStringToBits(addrMask)
		if err != nil || mask > 32 {
			mask = int32(32)
		}
	}

	if _, ok := ipv4["Address"]; ok {
		err = ipv4["Address"].Store(&addr)
		if err != nil {
			nm.logger.Println("error getting Address properties:", err)
		}
	}

	if _, ok := ipv4["Gateway"]; ok {
		err = ipv4["Gateway"].Store(&gateway)
		if err != nil {
			nm.logger.Println("error getting Gateway properties:", err)
		}
	}

	if addr != "" {
		conf.Ips = []nmutils.Ip{{Addr: addr, Mask: mask}}
	}
	if gateway != "" {
		conf.Gw = []nmutils.Ip{{Addr: gateway, Mask: mask}}
	}

	conf.DnsAddrs = []nmutils.Ip{}
	for _, v := range dns {
		value := ""
		err = v.Store(&value)
		if err == nil {
			conf.DnsAddrs = append(conf.DnsAddrs, nmutils.Ip{Addr: value})
		}
	}

	return conf, err
}

func getAvailableNetworks(nm *ConnmanDbus, conn *dbus.Conn, useCache bool) ([]nmutils.WifiConn, error) {
	nm.logger.Println("Scaning the network for access points")

	if useCache && netCache != nil && len(netCache) > 0 {
		nm.logger.Println("Getting from cache")

		networks := []string{}
		for _, conn := range netCache {
			networks = append(networks, fmt.Sprintf("%s (%d)", conn.Name, conn.Strength))
		}

		nm.logger.Println("Networks: ", strings.Join(networks, ", "))

		return netCache, nil
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(wifiPath))
	call := obj.Call("net.connman.Technology.Scan", 0)
	if call.Err != nil {
		nm.logger.Println("Failed to net.connman.Technology.Scan:", call.Err)
		return nil, call.Err
	}

	var response [][]interface{}
	obj = conn.Object(dbusDest, dbus.ObjectPath("/"))
	err := obj.Call("net.connman.Manager.GetServices", 0).Store(&response)
	if err != nil {
		nm.logger.Println("Failed to net.connman.Manager.GetServices:", err)
		return nil, err
	}

	conns := []nmutils.WifiConn{}
	for _, v := range response {
		if len(v) < 2 {
			continue
		}
		id := v[0].(dbus.ObjectPath)
		details := v[1].(map[string]dbus.Variant)
		security := []string{}
		strength := int32(0)
		favorite := false
		autoconnect := false
		name := ""
		interfacetype := ""

		if name, ok := details["Name"]; !ok || name.Value() == nil {
			continue
		}
		if _, ok := details["Type"]; ok {
			details["Type"].Store(&interfacetype)
			if interfacetype != "wifi" {
				continue
			}
		}

		if _, ok := details["Security"]; ok {
			details["Security"].Store(&security)
		}
		if _, ok := details["Strength"]; ok {
			details["Strength"].Store(&strength)
		}
		if _, ok := details["Favorite"]; ok {
			details["Favorite"].Store(&favorite)
		}
		if _, ok := details["AutoConnect"]; ok {
			details["AutoConnect"].Store(&autoconnect)
		}
		if _, ok := details["Name"]; ok {
			details["Name"].Store(&name)
		}

		conn := nmutils.WifiConn{
			ID:       string(id),
			Name:     name,
			Security: security,
			Strength: strength,
			Saved:    autoconnect || favorite,
		}

		conns = append(conns, conn)
	}

	netCache = conns

	networks := []string{}
	for _, conn := range conns {
		networks = append(networks, fmt.Sprintf("%s (%d)", conn.Name, conn.Strength))
	}

	nm.logger.Println("Networks: ", strings.Join(networks, ", "))

	return conns, nil
}

func startInterface(nm *ConnmanDbus, conn *dbus.Conn) error {
	nm.logger.Println("Starting wifi...")

	obj := conn.Object(dbusDest, dbus.ObjectPath(wifiPath))
	call := obj.Call("net.connman.Technology.SetProperty", 0, "Powered", dbus.MakeVariant(true))
	if call.Err != nil && call.Err.Error() != "Already enabled" {
		nm.logger.Println("Failed to start wifi interface:", call.Err)
		return call.Err
	}

	return nil
}

func connectToWifi(nm *ConnmanDbus, conn *dbus.Conn, ssid, pass string) error {
	network := nmutils.WifiConn{}
	for _, val := range netCache {
		if val.Name == ssid {
			network = val
		}
	}

	if network.ID == "" {
		return errors.New("network not found")
	}

	nm.logger.Printf("creating connection to network: %+v\n", network)

	err := createAgent(nm, conn, pass)
	if err != nil {
		nm.logger.Printf("error getting agent: %s\n", err.Error())
		return err
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(network.ID))
	call := obj.Call("net.connman.Service.Connect", 0)
	if call.Err != nil && call.Err.Error() != "Already connected" {
		nm.logger.Println("Failed to connect to wifi:", ssid)
		return call.Err
	}

	// disable powersave on connman (not working)
	// call = obj.Call("org.freedesktop.DBus.Properties.Set", 0, "net.connman.Service", "IdleTimeout", dbus.MakeVariant(uint32(0)))
	// if call.Err != nil {
	// 	nm.logger.Println("Failed to set powersave off:", call.Err)
	// 	// return call.Err
	// }

	// err = removeAgent(nm, conn)
	// if err != nil {
	// 	return err
	// }

	connectedTo = &network

	return nil
}

func createAgent(nm *ConnmanDbus, conn *dbus.Conn, pass string) error {
	reply, err := conn.RequestName(agentName, dbus.NameFlagDoNotQueue)
	if err != nil {
		return err
	}

	switch reply {
	case dbus.RequestNameReplyAlreadyOwner:
		msg := "agent -- RequestNameReplyAlreadyOwner"
		nm.logger.Println(msg)
		err = errors.New(msg)
	case dbus.RequestNameReplyExists:
		msg := "agent -- RequestNameReplyExists"
		nm.logger.Println(msg)
		err = nil
	case dbus.RequestNameReplyInQueue:
		msg := "agent -- RequestNameReplyInQueue"
		nm.logger.Println(msg)
		err = errors.New(msg)
	case dbus.RequestNameReplyPrimaryOwner:
		msg := "agent -- RequestNameReplyPrimaryOwner"
		nm.logger.Println(msg)
		err = nil
	default:
		err = fmt.Errorf("can't create agent")
	}

	if err != nil {
		return err
	}

	agent := &Agent{
		Name:       agentName,
		Path:       dbus.ObjectPath(agentPath),
		Interface:  agentInterface,
		Passphrase: pass,
	}

	err = conn.Export(agent, agent.Path, agentInterface)
	if err != nil {
		nm.logger.Printf("export agent error: %s\n", err.Error())
		return err
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath("/"))
	call := obj.Call(manager+".RegisterAgent", 0, agent.Path)
	if call.Err != nil {
		nm.logger.Printf("RegisterAgen error: %s\n", call.Err.Error())
		return call.Err
	}

	return nil
}

// func removeAgent(nm *ConnmanDbus, conn *dbus.Conn) error {
// 	obj := conn.Object(dbusDest, dbus.ObjectPath("/"))
// 	call := obj.Call(manager+".UnregisterAgent", 0, agentPath)
// 	if call.Err != nil {
// 		nm.logger.Println("error on UnregisterAgent: ", call.Err.Error())
// 		return call.Err
// 	}

// 	reply, err := conn.ReleaseName(agentName)
// 	if err != nil {
// 		return err
// 	}

// 	switch reply {
// 	case dbus.ReleaseNameReplyNonExistent:
// 		msg := "agent -- ReleaseNameReplyNonExistent"
// 		nm.logger.Println(msg)
// 		err = errors.New(msg)
// 	case dbus.ReleaseNameReplyNotOwner:
// 		msg := "agent -- ReleaseNameReplyNotOwner"
// 		nm.logger.Println(msg)
// 		err = errors.New(msg)
// 	case dbus.ReleaseNameReplyReleased:
// 		msg := "agent -- ReleaseNameReplyReleased"
// 		nm.logger.Println(msg)
// 		err = nil
// 	default:
// 		err = fmt.Errorf("can't create agent")
// 	}

// 	if err != nil {
// 		return err
// 	}

// 	err = conn.Export(nil, agentPath, agentInterface)
// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }

func netmaskStringToBits(netmask string) (int32, error) {
	ip := net.ParseIP(netmask)
	if ip == nil {
		return 0, fmt.Errorf("invalid netmask: %s", netmask)
	}
	ones, _ := ip.DefaultMask().Size()
	return int32(ones), nil
}
