package connman

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

var connectedTo *nmutils.WifiConn

type ConnmanDbus struct {
	logger     *log.Logger
	serverPort string

	Networks []nmutils.WifiConn
}

func New(logger *log.Logger, port string) *ConnmanDbus {
	return &ConnmanDbus{
		serverPort: port,
		logger:     logger,
	}
}

func (nm *ConnmanDbus) GetAvailableWiFiNetworks(rescan bool, waitRescan bool) ([]nmutils.WifiConn, error) {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return nil, err
	}
	defer conn.Close()

	tethering := false
	getWifiProperty(nm, conn, "Tethering", &tethering)
	if tethering {
		rescan = false
		// stopAp(nm, conn)
	}

	conns, err := getAvailableNetworks(nm, conn, !rescan)
	if err != nil {
		nm.logger.Println("Failed to getAvailableNetworks:", err)
		return nil, err
	}

	// if tethering {
	// startAp(nm, conn, nmutils.SSID, pass)
	// }

	return conns, nil
}

func (nm *ConnmanDbus) EnableInterface(name string) error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	err = startInterface(nm, conn)
	if err != nil {
		nm.logger.Println("Failed to startInterface:", err)
		return err
	}

	return nil
}

func (nm *ConnmanDbus) ScanAvailableWiFiNetworks() error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	obj := conn.Object(dbusDest, dbus.ObjectPath(wifiPath))
	call := obj.Call("net.connman.Technology.Scan", 0)
	if call.Err != nil {
		nm.logger.Println("Failed to net.connman.Technology.Scan:", call.Err)
		return call.Err
	}

	return nil
}

func (nm *ConnmanDbus) ConnectWiFiNetwork(name string, pass string, freq int, auto bool) error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	tethering := false
	getWifiProperty(nm, conn, "Tethering", &tethering)

	if tethering {
		err = stopAp(nm, conn)
		if err != nil {
			nm.logger.Println("error stopAp: ", err)
			return startAp(nm, conn, nmutils.SSID, nmutils.PASS)
		}
	}

	for i := 0; i < 10; i++ {
		_, err = getAvailableNetworks(nm, conn, false)
		if err != nil {
			time.Sleep(time.Second / 2)
			nm.logger.Println("waiting 1 second to rescan")
			continue
		}

		break
	}

	if err != nil {
		nm.logger.Println("error getAvailableNetworks: ", err)
		if tethering {
			return startAp(nm, conn, nmutils.SSID, nmutils.PASS)
		} else {
			return err
		}
	}

	for i := 0; i < 10; i++ {
		err = connectToWifi(nm, conn, name, pass)
		if err != nil {
			time.Sleep(time.Second / 2)
			nm.logger.Println("waiting 1 second to reconnect")
			continue
		}

		break
	}

	if err != nil {
		nm.logger.Println("error connectToWifi: ", err)
		return startAp(nm, conn, nmutils.SSID, nmutils.PASS)
	}

	connected := false
	for i := 0; i < 10; i++ {
		getWifiProperty(nm, conn, "Connected", &connected)
		if connected {
			break
		}
		time.Sleep(time.Second / 2)
	}

	if connected {
		nm.logger.Println("Connected to:", name)
		return nil
	}

	nm.logger.Println("Could not connect to:", name)
	if tethering {
		nm.logger.Println("Starting AP again:", name)
		return startAp(nm, conn, nmutils.SSID, nmutils.PASS)
	}

	return errors.New("could not connect to network " + name)
}

func (nm *ConnmanDbus) ConnectToStoredWiFiNetwork(name string) error {
	fmt.Println("not implemented ConnectToStoredWiFiNetwork") // TODO: Implement

	return nil
}

func (nm *ConnmanDbus) GetInterfaceIpConf(name string) (*nmutils.IPConfiguration, error) {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return nil, err
	}
	defer conn.Close()

	return getWifiIp(nm, conn)
}

func (nm *ConnmanDbus) RemoveStoredWiFiNetwork(name string) error {
	fmt.Println("not implemented RemoveStoredWiFiNetwork") // TODO: Implement

	return nil
}

func (nm *ConnmanDbus) GetStoredWiFiNetworks() ([]string, error) {
	fmt.Println("not implemented GetStoredWiFiNetworks") // TODO: Implement

	return nil, nil
}

func (nm *ConnmanDbus) StopAp() error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	err = stopAp(nm, conn)

	return err
}

func (nm *ConnmanDbus) StartAp(name string, pass string) error {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	err = startAp(nm, conn, name, pass)
	if err != nil {
		nm.logger.Println("Failed starting ap:", err)
		return err
	}

	return nil
}

func (nm *ConnmanDbus) InitNetwork(watch bool, interval time.Duration) error {
	nm.logger.Println("Starting InitNetwork protocol")
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		nm.logger.Println("Failed to connect to session bus:", err)
		return err
	}
	defer conn.Close()

	connected := false
	tethering := false

	for i := 0; i < 10; i++ {
		err = startInterface(nm, conn)
		if err != nil {
			time.Sleep(time.Second / 2)
			continue
		}
		break
	}

	for i := 0; i < 10; i++ {
		err = getWifiProperty(nm, conn, "Tethering", &tethering)
		if err != nil {
			time.Sleep(time.Second / 2)
			continue
		}
		break
	}

	for i := 0; i < 10; i++ {
		if tethering {
			err := stopAp(nm, conn)
			if err != nil {
				time.Sleep(time.Second / 2)
				continue
			}
		}
		break
	}

	if err != nil {
		nm.logger.Println("Failed stop ap:", err)
		return err
	}

	for i := 0; i < 10; i++ {
		err = startInterface(nm, conn)
		if err != nil {
			time.Sleep(time.Second / 2)
			continue
		}
		break
	}

	if err != nil {
		nm.logger.Println("Failed start wifi interface:", err)
		return err
	}

	for i := 0; i < 10; i++ {
		_, err = getAvailableNetworks(nm, conn, false)
		if err != nil {
			time.Sleep(time.Second / 2)
			continue
		}
		break
	}

	if err != nil {
		nm.logger.Println("Failed getAvailableNetworks:", err)
		return err
	}

	for i := 0; i < 10; i++ {
		err = getWifiProperty(nm, conn, "Connected", &connected)
		if err != nil || !connected {
			if !connected {
				nm.logger.Printf("waiting for wifi to get connected %d\n", i)
			}
			time.Sleep(time.Second / 2)
			continue
		}
		break
	}

	if !connected {
		err = startAp(nm, conn, nmutils.SSID, nmutils.PASS)
		if err != nil {
			nm.logger.Println("Failed startAp:", err)
			return err
		}

		nm.logger.Println("No network configuration found, creating hotspot", nmutils.SSID)
		nm.logger.Println("Connect to network using the password: ", nmutils.PASS)
		nm.logger.Printf("You can now enter to: http://192.168.123.1%s\n", nm.serverPort)
	}

	return nil
}
