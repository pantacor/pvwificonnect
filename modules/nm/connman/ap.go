package connman

import (
	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/firewall"
)

func stopAp(nm *ConnmanDbus, conn *dbus.Conn) error {
	obj := conn.Object(dbusDest, dbus.ObjectPath(wifiPath))
	call := obj.Call("net.connman.Technology.SetProperty", 0, "Tethering", dbus.MakeVariant(false))
	if call.Err != nil {
		nm.logger.Println(call.Err)
		return nil
	}

	err := firewall.DeactiveCaptive()
	if err != nil {
		nm.logger.Printf("Error when DeactiveCaptive -- %s\n", err)
		return nil
	}

	return nil
}

func startAp(nm *ConnmanDbus, conn *dbus.Conn, ssid, pass string) error {
	apEnabled := false
	powered := false
	getWifiProperty(nm, conn, "Tethering", &apEnabled)
	getWifiProperty(nm, conn, "Powered", &powered)

	if apEnabled && powered {
		return nil
	}

	obj := conn.Object(dbusDest, dbus.ObjectPath(wifiPath))
	if !powered {
		call := obj.Call("net.connman.Technology.SetProperty", 0, "Powered", dbus.MakeVariant(true))
		if call.Err != nil {
			nm.logger.Println(call.Err)
			return nil
		}
	}

	// Enable WiFi tethering
	call := obj.Call("net.connman.Technology.SetProperty", 0, "TetheringIdentifier", dbus.MakeVariant(ssid))
	if call.Err != nil {
		nm.logger.Println(call.Err)
		return nil
	}

	call = obj.Call("net.connman.Technology.SetProperty", 0, "TetheringPassphrase", dbus.MakeVariant(pass))
	if call.Err != nil {
		nm.logger.Println(call.Err)
		return nil
	}

	if !apEnabled {
		call = obj.Call("net.connman.Technology.SetProperty", 0, "Tethering", dbus.MakeVariant(true))
		if call.Err != nil {
			nm.logger.Println(call.Err)
			return nil
		}
	}

	err := firewall.ActivateCaptive()
	if err != nil {
		nm.logger.Printf("Error when ActivateCaptive -- %s\n", err)
		return nil
	}
	return nil
}
