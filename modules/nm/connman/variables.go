package connman

const (
	connmanService     = "net.connman"
	connmanPath        = "/net/connman/technology/wifi"
	connmanInterface   = "net.connman.Technology"
	tetheringInterface = "net.connman.Tethering"
	ID                 = "net.connman"
	wifiPath           = "/net/connman/technology/wifi"
	apPath             = "/net/connman/technology/p2p"
	dbusDest           = "net.connman"
	manager            = "net.connman.Manager"
	agentName          = "net.connman"
	agentPath          = "/test/agent"
	agentInterface     = "net.connman.Agent"
)
