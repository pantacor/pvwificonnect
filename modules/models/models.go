package models

import (
	"html/template"
	"log"

	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

type ServerOptions struct {
	Logger     *log.Logger
	TpmlEngine *template.Template
	Client     nmutils.NetworkManagerDbus
}
