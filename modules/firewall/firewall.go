package firewall

import (
	"fmt"
	"log"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

const (
	dbusDestination = "org.pantacor.PvWificonnect"
	dbusPath        = "/org/pantacor/PvWificonnect"
	dbusInterface   = "org.pantacor.PvWificonnect"
)

const IPv4 = "192.168.123.1/24"

var rulesToAdd = []string{
	// Clear existing rules
	"-F",
	"-t nat -F",
	"-t mangle -F",

	// Delete any user-defined chains that were created by iptable commands.
	"-X",
	"-t nat -X",
	"-t mangle -X",

	// Allow established and related connections
	"-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT",
	"-A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT",

	// Allow traffic on the loopback interface
	"-A INPUT -i lo -j ACCEPT",

	// Redirect all HTTP and HTTPS traffic to captive portal
	"-t nat -A PREROUTING -i tether -p tcp --dport 80 -j DNAT --to-destination 192.168.123.1:80",
	"-t nat -A PREROUTING -i tether -p tcp --dport 443 -j DNAT --to-destination 192.168.123.1:443",
	"-t nat -A PREROUTING -i tether -p udp --dport 53 -j DNAT --to-destination 192.168.123.1:53",
	"-t nat -A PREROUTING -i tether -p tcp --dport 53 -j DNAT --to-destination 192.168.123.1:53",

	// Allow incoming traffic on tether for specific services
	"-A INPUT -i tether -p tcp --dport 80 -j ACCEPT",
	"-A INPUT -i tether -p tcp --dport 443 -j ACCEPT",
	"-A INPUT -i tether -p udp --dport 53 -j ACCEPT",
	"-A INPUT -i tether -p tcp --dport 53 -j ACCEPT",
	"-A INPUT -i tether -p udp --dport 67 -j ACCEPT",
	"-A INPUT -i tether -p tcp --dport 8222 -j ACCEPT",
	"-A INPUT -i tether -p tcp --dport 12368 -j ACCEPT",
	"-A INPUT -i tether -p tcp --dport 12369 -j ACCEPT",

	// Enable NAT
	"-t nat -A POSTROUTING -o eth0 -j MASQUERADE",

	// Allow forwarding from tether to eth0
	"-A FORWARD -i tether -o eth0 -j ACCEPT",

	// Allow forwarding from eth0 to tether for established connections
	"-A FORWARD -i eth0 -o tether -m state --state ESTABLISHED,RELATED -j ACCEPT",

	// Allow outgoing traffic
	"-A OUTPUT -j ACCEPT",

	// Set default policies to ACCEPT
	"-P INPUT ACCEPT",
	"-P FORWARD ACCEPT",
	"-P OUTPUT ACCEPT",

	// Allow all traffic on eth0
	"-A INPUT -i eth0 -j ACCEPT",
	"-A OUTPUT -o eth0 -j ACCEPT",
}

var rulesToRemove = []string{
	// Flush all existing rules (both filter and nat tables)
	"-F",
	"-t nat -F",
	"-t mangle -F",

	// Delete any user-defined chains that were created by iptable commands.
	"-X",
	"-t nat -X",
	"-t mangle -X",

	// Set default policies to ACCEPT for INPUT, FORWARD, and OUTPUT chains
	"-P INPUT ACCEPT",
	"-P FORWARD ACCEPT",
	"-P OUTPUT ACCEPT",

	// Allow established connections (incoming traffic that matches an existing outgoing connection)
	"-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT",
	"-A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT",

	// Allow all traffic on eth0 interface
	"-A INPUT -i eth0 -j ACCEPT",
	"-A OUTPUT -o eth0 -j ACCEPT",

	// Allow all traffic on wlan0 interface
	"-A INPUT -i wlan0 -j ACCEPT",
	"-A OUTPUT -o wlan0 -j ACCEPT",
}

func ActivateCaptive() error {
	conn, err := dbus.SystemBus()
	if err != nil {
		log.Printf("Failed to connect to system bus: %v\n", err)
		return err
	}

	fmt.Printf("Trying to add all captive portal rules\n")
	isAvailable := pvwificonnectDbusIsAvailable()
	if !isAvailable {
		return fmt.Errorf("pvwificonnect Dbus is not available")
	}

	obj := conn.Object(dbusDestination, dbus.ObjectPath(dbusPath))

	for _, rule := range rulesToAdd {
		call := obj.Call(dbusInterface+".AddFwRule", 0, rule)
		if call.Err != nil {
			return fmt.Errorf("failed to add rule: %v\nRule: %v", call.Err, rule)
		}
	}

	call := obj.Call(dbusInterface+".SetTetherIPv4", 0, IPv4)
	if call.Err != nil {
		return fmt.Errorf("failed to set tether IPv4: %s -- %v", call.Err, IPv4)
	}

	fmt.Printf("Successfully added all captive portal rules\n")
	return nil
}

func DeactiveCaptive() error {
	conn, err := dbus.SystemBus()
	if err != nil {
		log.Printf("Failed to connect to system bus: %v\n", err)
		return err
	}

	fmt.Printf("Trying to remove all captive portal rules\n")
	isAvailable := pvwificonnectDbusIsAvailable()
	if !isAvailable {
		return fmt.Errorf("pvwificonnect Dbus is not available")
	}

	obj := conn.Object(dbusDestination, dbus.ObjectPath(dbusPath))
	for _, rule := range rulesToRemove {
		call := obj.Call(dbusInterface+".AddFwRule", 0, rule)
		if call.Err != nil {
			return fmt.Errorf("failed to remove rule: %v\nRule: %v", call.Err, rule)
		}
	}

	fmt.Printf("Successfully removed all captive portal rules\n")
	return nil
}

func pvwificonnectDbusIsAvailable() bool {
	conn, err := nmutils.GetSystemDbus()
	if err != nil {
		return false
	}
	defer conn.Close()

	var s []string
	isAvailable := false

	// wait 20 seconds
	for i := 0; i < 20; i++ {
		obj := conn.Object("org.freedesktop.DBus", dbus.ObjectPath("/org/freedesktop/DBus"))
		err = obj.Call("org.freedesktop.DBus.ListNames", 0).Store(&s)

		for _, service := range s {
			if service == dbusDestination {
				return true
			}
		}

		if err != nil || !isAvailable {
			fmt.Println("waiting for pvwificonnect dbus to be ready")
			time.Sleep(1 * time.Second)
			continue
		}
	}

	if !isAvailable {
		fmt.Printf("can not load %s from DBus", dbusDestination)
		fmt.Printf("org.freedesktop.DBus.ListNames: %+v\n", s)
	}

	return isAvailable
}
