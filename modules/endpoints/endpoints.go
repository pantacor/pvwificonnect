package endpoints

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/pantacor/pvwificonnect/modules/models"
	"gitlab.com/pantacor/pvwificonnect/modules/nm/nmutils"
)

const (
	ContentTypeJson = "application/json"
)

const (
	SsidIsEmpty           = "ssid can't be empty"
	PasswordIsEmpty       = "password can't be empty"
	SsidOrPasswordIsEmpty = "ssid or password can't be empty"
)

type ConnectionPost struct {
	SSID      string `json:"ssid"`
	Password  string `json:"password"`
	Frequency int    `json:"frequency"`
}

type ResponseData struct {
	Connections      []nmutils.WifiConn       `json:"connections"`
	NetworkConnected string                   `json:"connecto-to"`
	IpConfiguration  *nmutils.IPConfiguration `json:"ipconfiguration"`
	Error            error                    `json:"error"`
	Code             int                      `json:"code"`
}

type ResponseDataJSON struct {
	Connections      []nmutils.WifiConn       `json:"connections,omitempty"`
	NetworkConnected string                   `json:"connecto-to,omitempty"`
	IpConfiguration  *nmutils.IPConfiguration `json:"ipconfiguration,omitempty"`
	Error            string                   `json:"error,omitempty"`
	Code             int                      `json:"code,omitempty"`
}

var availableFormats []string = []string{"json", "html", "css"}

func RedirectToConnect(opts *models.ServerOptions) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/connect", http.StatusFound)
		return
	})
}

func HandleIndex(opts *models.ServerOptions) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		default:
			getIndex(w, r, opts)
			return
		}
	})
}

func HandleSuccess(opts *models.ServerOptions) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := &ResponseData{}
		render(w, r, opts, "success.html", data)
		return
	})
}

func HandleConnections(opts *models.ServerOptions) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "POST":
			postNetwork(w, r, opts)
			return
		default:
			getConnections(w, r, opts)
			return
		}
	})
}

func getConnections(w http.ResponseWriter, r *http.Request, opts *models.ServerOptions) {
	data := ResponseData{}

	view := "index.html"
	if r.Header.Get("Hx-Request") != "" && r.Header.Get("Hx-Boosted") == "" {
		view = "connections"
	}

	conns, err := opts.Client.GetAvailableWiFiNetworks(true, false)
	if err != nil {
		opts.Logger.Println(err.Error())
		data.Error = err
		render(w, r, opts, view, &data)
		return
	}

	data.Connections = conns
	render(w, r, opts, view, &data)
}

func getIndex(w http.ResponseWriter, r *http.Request, opts *models.ServerOptions) {
	data := ResponseData{}
	conns, err := opts.Client.GetAvailableWiFiNetworks(false, false)
	if err != nil {
		opts.Logger.Println(err.Error())
		data.Error = err
		render(w, r, opts, "index.html", &data)
		return
	}

	data.Connections = conns
	connected := ""
	for _, conn := range conns {
		if conn.Saved {
			connected = conn.Name
			break
		}
	}

	if connected != "" {
		data.NetworkConnected = connected
		opts.Logger.Printf("Getting wlan0 IP configuration: %+v \n", connected)
		conf, err := opts.Client.GetInterfaceIpConf("wlan0")
		if err != nil {
			opts.Logger.Println(err.Error())
			data.Error = err
		}
		data.IpConfiguration = conf
		opts.Logger.Printf("wlan0 IP configuration: %+v \n", conf)
	}

	view := "index.html"
	render(w, r, opts, view, &data)
}

func postNetwork(w http.ResponseWriter, r *http.Request, opts *models.ServerOptions) {
	view := "index.html"
	data := ResponseData{
		Connections:      []nmutils.WifiConn{},
		NetworkConnected: "",
	}
	payload := &ConnectionPost{}
	if r.Header.Get("Content-Type") == ContentTypeJson {
		data.Error = json.NewDecoder(r.Body).Decode(payload)
	} else {
		data.Error = r.ParseForm()
		values := strings.Split(strings.Trim(r.Form.Get("ssid"), " "), "---")
		payload.Password = strings.Trim(r.Form.Get("password"), " ")
		payload.SSID = values[0]
		if len(values) == 2 {
			f, err := strconv.Atoi(values[1])
			if err == nil {
				payload.Frequency = f
			}
		}
	}
	if data.Error != nil {
		opts.Logger.Println(data.Error.Error())
		render(w, r, opts, view, &data)
		return
	}

	if payload.SSID == "" || payload.Password == "" {
		data.Error = errors.New(SsidOrPasswordIsEmpty)
		data.Code = http.StatusBadRequest
		opts.Logger.Println(data.Error.Error())
		render(w, r, opts, view, &data)
		return
	}

	alreadyRender := false
	if strings.Contains(r.Host, "192.168.123.1") || strings.Contains(r.Host, "localhost") {
		render(w, r, opts, "success.html", &data)
		alreadyRender = true
	}

	opts.Logger.Println("Saving Network")
	data.Error = saveNetwork(payload, opts.Client, opts.Logger)
	if data.Error != nil {
		if !alreadyRender {
			render(w, r, opts, view, &data)
		}
		return
	}

	data.NetworkConnected = payload.SSID
	if payload.Frequency == 0 {
		opts.Logger.Printf("Setting Network connected as : %+v \n", payload.SSID)
	} else {
		opts.Logger.Printf("Setting Network connected as : %+v - %d \n", payload.SSID, payload.Frequency)
	}
	opts.Logger.Printf("Getting wlan0 IP configuration: %+v \n", payload.SSID)
	conf, err := opts.Client.GetInterfaceIpConf("wlan0")
	if err != nil {
		opts.Logger.Println(err.Error())
		data.Error = err
	}
	data.IpConfiguration = conf

	opts.Logger.Printf("wlan0 IP configuration: %+v \n", conf)

	if !alreadyRender {
		render(w, r, opts, view, &data)
	}
}

func getFormat(c string) string {
	format := c
	for _, f := range availableFormats {
		if strings.Contains(c, f) {
			format = f
		}
	}
	return format
}

func saveNetwork(payload *ConnectionPost, client nmutils.NetworkManagerDbus, logger *log.Logger) (err error) {
	err = client.ConnectWiFiNetwork(payload.SSID, payload.Password, payload.Frequency, true)
	if err != nil {
		return err
	}

	err = client.StopAp()
	return err
}

func render(w http.ResponseWriter, r *http.Request, opts *models.ServerOptions, name string, data *ResponseData) {
	if data.Error != nil {
		opts.Logger.Println(data.Error.Error())
	}
	contentType := r.Header.Get("Accept")
	format := getFormat(contentType)

	switch format {
	case "json":
		enc := json.NewEncoder(w)
		w.Header().Set("Content-Type", ContentTypeJson)
		if data.Code > 0 {
			w.WriteHeader(data.Code)
		}
		response := &ResponseDataJSON{
			Connections:      data.Connections,
			IpConfiguration:  data.IpConfiguration,
			NetworkConnected: data.NetworkConnected,
			Code:             data.Code,
		}
		if data.Error != nil {
			response.Error = data.Error.Error()
		}
		err := enc.Encode(response)
		if err != nil {
			opts.Logger.Println(err.Error())
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}
	default:
		err := opts.TpmlEngine.ExecuteTemplate(w, name, data)
		if err != nil {
			opts.Logger.Println(err.Error())
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}
	}
}
