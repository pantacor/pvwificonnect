package main

import (
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/pantacor/pvwificonnect/modules/nm"
	"gitlab.com/pantacor/pvwificonnect/modules/server"
)

var (
	listenAddr      string
	httpsListenAddr string
)

func main() {
	flag.StringVar(&listenAddr, "listen-addr", ":80", "server listen address")
	flag.StringVar(&httpsListenAddr, "https-listen-addr", ":443", "https server listen address")
	flag.Parse()

	listenAddr = strings.Trim(listenAddr, " ")
	httpsListenAddr = strings.Trim(httpsListenAddr, " ")

	logger := log.New(os.Stdout, "pvwificonnect: ", log.LstdFlags)

	typeOfDbus, err := nm.GetAvailableDbus()
	if err != nil {
		logger.Printf("%v\n", err)
		os.Exit(1)
	}

	nmClient, err := nm.NewNetworkManagerDbus(typeOfDbus, logger, listenAddr)
	if err != nil {
		logger.Printf("%v\n", err)
		os.Exit(1)
	}

	watcher := os.Getenv("PV_WIFI_CONNECT_WATCHER")
	watch := false
	if watcher != "" {
		watch = true
	}
	intervalString := os.Getenv("PV_WIFI_CONNECT_INTERVAl")
	interval := 1 * time.Second
	if intervalString != "" {
		inter, err := time.ParseDuration(intervalString)
		if err == nil {
			interval = inter
		}
	}

	go func() {
		if err = nmClient.InitNetwork(watch, interval); err != nil {
			logger.Printf("%v\n", err)
		}
	}()

	paths := []string{
		filepath.Join("templates", "layout.html"),
		filepath.Join("templates", "index.html"),
		filepath.Join("templates", "error.html"),
		filepath.Join("templates", "success.html"),
		filepath.Join("templates", "_signal.html"),
		filepath.Join("templates", "_frequency.html"),
	}

	conf := &server.ServerConfig{
		ListenAddr:      listenAddr,
		HttpsListenAddr: httpsListenAddr,
		Logger:          logger,
		NmClient:        nmClient,
		TemplatePaths:   paths,
	}

	server.Serve(conf)
}
