#!/bin/sh
set -e

echo "Downloading busybox for $TARGETPLATFORM"

busyboxfile=""
qemufile=""
case "$TARGETPLATFORM" in
"linux/arm/v6"*)
	busyboxfile="busybox-armv5l"
	qemufile="qemu-arm-static"
	;;
"linux/arm/v7"*)
	busyboxfile="busybox-armv7l"
	qemufile="qemu-arm-static"
	;;
"linux/arm64"*)
	busyboxfile="busybox-armv8l"
	qemufile="qemu-aarch64-static"
	;;
"linux/386"*)
	busyboxfile="busybox-i686"
	qemufile="qemu-i386-static
"
	;;
"linux/amd64"*)
	busyboxfile="busybox-x86_64"
	qemufile="qemu-x86_64-static"
	;;
"linux/mips"*)
	busyboxfile=" busybox-mips"
	qemufile="qemu-mips-static"
	;;
"linux/mipsle"*)
	busyboxfile="busybox-mipsel"
	qemufile="qemu-mipsel-static"
	;;
"linux/mips64"*)
	busyboxfile="busybox-mips64"
	qemufile="qemu-mips64-static"
	;;
"linux/mips64le"*)
	busyboxfile="busybox-mips64"
	qemufile="qemu-mips64el-static"
	;;
*)
	echo "Unknown machine type: $machine"
	exit 1
	;;
esac

url="https://busybox.net/downloads/binaries/1.31.0-defconfig-multiarch-musl/${busyboxfile}"
echo "Downloading from $url"
wget ${url} -O busybox
chmod +x busybox

mkdir -p qemu 
wget -O qemu/${qemufile} https://github.com/multiarch/qemu-user-static/releases/download/v7.2.0-1/${qemufile}
chmod a+x qemu/${qemufile}
